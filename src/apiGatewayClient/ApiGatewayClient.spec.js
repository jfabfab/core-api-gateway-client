/* global expect loggerForTests */

const proxyquire = require('proxyquire');
const { EventEmitter } = require('events');


describe('Test Suite for Gateway', () => {
  let requestMock;
  let ApiGatewayClient;
  let constructorArgs;

  beforeEach(() => {
    requestMock = new EventEmitter();
    requestMock.post = jasmine.createSpy('post').and.callFake(() => {
      setImmediate(() => {
        const responseMock = new EventEmitter();
        requestMock.emit('response', responseMock);
        responseMock.emit('data', JSON.stringify({ statusCode: 200 }));
        responseMock.emit('end');
      });
      return requestMock;
    });

    constructorArgs = {
      baseRoute: 'http://test-gateway-endpoint',
      services: {
        testService: 'test-service-name',
      },
      logger: loggerForTests,
      retryCount: 1,
    };

    ApiGatewayClient = proxyquire('./ApiGatewayClient', {
      request: requestMock,
    }).ApiGatewayClient;
  });

  describe('Class Initialization', () => {
    function entryError(desc, {
      expectedErr,
      params,
    }) {
      it(desc, () => {
        expect(() => new ApiGatewayClient(params)).toThrow(
          jasmine.stringMatching(expectedErr)
        );
      });
    }

    function entry(desc, {
      testFn,
      params,
    }) {
      it(desc, () => {
        // run
        const apiGatewayClient = new ApiGatewayClient(params);

        // test
        testFn(apiGatewayClient);
      });
    }

    entryError('Should handle services', {
      expectedErr: /services/,
      params: {
        baseRoute: 'http://test-gateway-endpoint',
      },
    });

    entry('Should not enable service when baseRoute is not passed', {
      testFn: (client) => {
        expect(client.enabled).toBe(false);
      },
      params: {
        services: {
          testService: 'test-service-name',
        },
      },
    });

    entry('Should set retryCount to default when it is not passed', {
      params: {
        baseRoute: 'http://test-gateway-endpoint',
        services: {
          testService: 'test-service-name',
        },
      },
      testFn: (client) => {
        expect(typeof client.retryCount).toEqual('number');
        expect(client.retryCount > 0).toBe(true);
      },
    });

    entry('Should set retryCount to value from arguments', {
      params: {
        baseRoute: 'http://test-gateway-endpoint',
        services: {
          testService: 'test-service-name',
        },
        retryCount: 3,
      },
      testFn: (client) => {
        expect(client.retryCount).toEqual(3);
      },
    });
  });

  describe('send method', () => {
    describe('errors handling', () => {
      function entry({
        desc,
        prepareFn,
        testFn,
      }) {
        it(desc, async () => {
          // prepare
          prepareFn();

          const client = new ApiGatewayClient(constructorArgs);

          // run
          async function run() {
            const params = {
              action: 'test-action',
              payload: {},
              service: 'testService',
              target: 'test-target',
            };

            return client.send(params);
          }

          // test
          try {
            await run();
          } catch (e) {
            testFn(e);
          }
        });
      }

      entry({
        desc: 'Should handle internal errors while reporting',
        prepareFn: () => {
          requestMock.post = jasmine.createSpy('post').and.callFake(() => {
            const errorMock = new Error('connect ECONNREFUSED 0.0.0.0:3000');
            setImmediate(() => {
              requestMock.emit('error', errorMock);
            });
            return requestMock;
          });
        },
        testFn: (error) => {
          expect(error.message).toContain('ECONNREFUSED');
        },
      });

      entry({
        desc: 'Should handle errors in response from gateway',
        prepareFn: () => {
          requestMock.post = jasmine.createSpy('post').and.callFake(() => {
            const responseMock = new EventEmitter();
            setImmediate(() => {
              requestMock.emit('response', responseMock);
              responseMock.emit('data', JSON.stringify({ errors: { code: 403, message: 'Forbidden' } }));
              responseMock.emit('end');
            });
            return requestMock;
          });
        },
        testFn: (error) => {
          expect(error.code).toContain('SERVICE_ERROR');
          expect(error.message).toEqual('Forbidden');
          expect(error.httpStatusCode).toEqual(403);
        },
      });
    });

    it('Should make send payload to apigateway endpoint', async () => {
      // prepare
      requestMock.post = jasmine.createSpy('post').and.callFake(() => {
        const responseMock = new EventEmitter();
        setImmediate(() => {
          requestMock.emit('response', responseMock);
          responseMock.emit('data', JSON.stringify({ result: true, data: { test: 'ok' } }));
          responseMock.emit('end');
        });
        return requestMock;
      });

      // run
      async function run() {
        const params = {
          action: 'test-action',
          payload: {
            email: 'test-email',
          },
          service: 'testService',
          target: 'test-target',
        };
        const client = new ApiGatewayClient(constructorArgs);
        return client.send(params);
      }

      // test
      function test(result) {
        const expectedResult = JSON.stringify({ result: true, data: { test: 'ok' } });
        const expectedJson = {
          json: {
            endpointParams: {
              service: 'test-service-name',
              action: 'test-action',
              target: 'test-target',
            },
            payload: {
              email: 'test-email',
            },
          },
        };
        const expectedEndpoint = {
          url: 'http://test-gateway-endpoint/test-service-name',
        };

        expect(JSON.stringify(result)).toEqual(expectedResult);
        expect(requestMock.post).toHaveBeenCalledWith(
          jasmine.objectContaining(expectedJson)
        );
        expect(requestMock.post).toHaveBeenCalledWith(
          jasmine.objectContaining(expectedEndpoint)
        );
      }

      const r = await run();
      test(r);
    });

    it('should handle when no data is returned', async () => {
      // prepare
      requestMock.post = jasmine.createSpy('post').and.callFake(() => {
        const responseMock = new EventEmitter();
        setImmediate(() => {
          requestMock.emit('response', responseMock);
          responseMock.emit('end');
        });
        return requestMock;
      });

      // run
      async function run() {
        const params = {
          action: 'test-action',
          payload: {
            email: 'test-email',
          },
          service: 'testService',
          target: 'test-target',
        };
        const client = new ApiGatewayClient(constructorArgs);
        return client.send(params);
      }

      // result
      function test(result) {
        expect(result).not.toBeDefined();
      }

      const r = await run();
      test(r);
    });


    it('should not call apigateway when service is not enabled', async () => {
      // prepare
      constructorArgs.baseRoute = ''; // disable
      const client = new ApiGatewayClient(constructorArgs);

      // run
      async function run() {
        return client.send({});
      }

      // result
      function test() {
        expect(client.enabled).toBe(false);
        expect(requestMock.post).not.toHaveBeenCalled();
      }

      await run();
      test();
    });
  });
});
