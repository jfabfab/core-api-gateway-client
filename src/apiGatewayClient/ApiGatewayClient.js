const assert = require('assert');
const retry = require('bluebird-retry');
const request = require('request');
const errors = require('../errors');

/* istanbul ignore next: consoleLogger */
const consoleLogger = {
  debug: console.log,
  log: console.log,
  info: console.info,
  warn: console.warn,
  error: console.error,
};

/* istanbul ignore next: silentLogger */
const silentLogger = {
  debug: () => {},
  log: () => {},
  info: () => {},
  warn: () => {},
  error: console.error,
};


class ApiGatewayClient {
  /**
   * Creates an instance of ApiGatewayClient.
   * @param {any} { endpoint, serviceName, serviceVersion, logger, retryCount = 5 }
   *
   * @memberof ApiGatewayClient
   */
  constructor({
    baseRoute,
    services,
    logger,
    retryCount = 2,
  }) {
    assert(services, 'User class expects services');

    this.enabled = !!baseRoute;
    this.baseRoute = baseRoute;
    this.services = services;
    this.retryCount = retryCount;

    /* istanbul ignore next: logging */
    if (logger === true) {
      this.logger = consoleLogger;
    } else if (logger) {
      this.logger = logger;
    } else {
      this.logger = silentLogger;
    }
  }


  /**
   * send request to API ApiGatewayClient
   * @param {any}
   * @returns {any}
   */
  async send({
    action,
    service,
    target,
    payload,
  }) {
    if (!this.enabled) {
      return Promise.resolve();
    }

    const json = this._formatBody({
      target,
      action,
      payload,
      serviceName: this.services[service],
    });

    const req = {
      url: `${this.baseRoute}/${json.endpointParams.service}`,
      method: 'POST',
      json,
      // qs: {
      //   access_token: 'xxxxx xxxxx' // -> uri + '?access_token=xxxxx%20xxxxx'
      // },
    };

    const retryOptions = {
      throw_original: true,
      context: this,
      interval: 1000,
      backoff: 2,
      max_tries: this.retryCount,
    };

    let retryCounter = 0;

    return Promise.resolve()
      .then(() =>
        retry(() => new Promise((resolve, reject) => {
          retryCounter += 1;
          this.logger.debug(`calling core-apigateway-service '${req.url}' (retry #${retryCounter})`);

          let data = '';

          request
            .post(req)
            .on('response', (response) => {
              let body;

              response.on('data', (chunk) => {
                // compressed data as it is received
                data += chunk;
              });

              response.on('end', () => {
                if (data) {
                  body = JSON.parse(data);
                }
                if (body && body.errors) {
                  reject(this._formatNonError(body.errors));
                }
                resolve(body);
              });
            })
            .on('error', (err) => {
              reject(err);
            });
        }), retryOptions))
      .catch((e) => {
        this.logger.error(`An error occured and we were unable to reach the api gateway. ${e.message}`);
        throw e;
      });
  }

  /**
   * format non-error from gateway response into appError
   *
   * @param {any} error
   * @returns {any} appError
   */
  _formatNonError({
    code,
    message,
    details = {},
  }) {
    const appError = new errors.AppError({
      code: 'ERR_TARGETED_SERVICE_ERROR',
      httpStatusCode: code,
      innerError: JSON.stringify(details),
      message,
    });

    return appError;
  }

  /**
   * format body content
   * @param {any} parameters
   * @returns {any} body
   */
  _formatBody({
    payload,
    action,
    serviceName,
    target,
  }) {
    const body = {
      endpointParams: {
        service: serviceName,
        target,
        action,
      },
      payload,
    };

    return body;
  }
}

module.exports = {
  ApiGatewayClient,
};
