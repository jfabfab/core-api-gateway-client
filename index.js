const { ApiGatewayClient } = require('./src/apiGatewayClient/ApiGatewayClient');

module.exports = {
  ApiGatewayClient,
};
