# API-GATEWAY CLIENT

## Intro

This npm package is for interacting with the [apigateway service](https://bitbucket.org/jfabfab/core-api-gateway/wiki/Home). It is used to communicate with the core-api-gateway service.


## Installation

````shell
$ npm install --save core-apigateway-client
````

## Sample usage

### Client initialization

```javascript
const { ApiGatewayClient } = require('core-apigateway-client');

const client = new ApiGatewayClient({
  baseRoute: 'http://localhost:5900', // optional (default: dev server)
  services: {
    // required services to call i.e.- serviceKey: 'service-name-as-value'  
  },
  retryCount: 6,            // optional
  logger: true,             // optional (true, false, winston)
});
```
You can then send any request to the core-apigateway service

````javascript
const service = 'security';
const action = 'authenticate';
const target = 'admin';
const payload = { email: 'me@example.com', password: '1234' }

try {
  const res = await client.send({
    service,
    action,
    target,
    payload,
  });
  // do something with res
} catch (error) {
  // do something with error
}

````
